<?php

// backend routes
Route::resource('/customers', 'CustomerController');

Route::resource('/categories', 'CategoryController');

Route::resource('/brands', 'BrandController');

Route::resource('/products', 'ProductController');

Route::resource('/carts', 'CartController');

// frontend routes
// Route::get('/accessories', function () {
    // return view('frontend.accessories.index');
// });
Route::resource('/carts', 'CartController');

Route::get('/accessories', 'FrontendController@frontIndex');
Route::get('/singleview/{id}', 'FrontendController@singleView');
// Route::get('/frontend/accessories/singleview', 'FrontendController@singleView');


//authentication

Route::get('/register', 'RegistrationController@register');
Route::post('/register', 'RegistrationController@postRegister');


Route::get('/login', 'LoginController@login');
Route::post('/login', 'LoginController@postLogin');

Route::post('/logout', 'LoginController@logout');

Route::get('/earnings', 'AdminController@earnings');




















// Route::resource('/customers', 'CustomerController');
// Route::get('/customers/activity/active_customers','SearchController@active_customers');
// Route::get('/customers/activity/inactive_customers','SearchController@inactive_customers');
