<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        //find all product info from Product model and the letest one will be in the top
        $tokens = Category::latest()->get();
        //for changing directory
        $directory = '/categories';
        //compact product's data and directory value into index route 
        return view('backend.categories.index', compact('tokens', 'directory'));
    }
    
    public function create()
    {
        //find all product info from Product model and the letest one will be in the top
        $tokens = Category::latest()->get();
        //for changing directory
        $directory = '/categories';
        //compact product's data and directory value into index route 
        return view('backend.categories.create', compact('tokens', 'directory'));
    }
    
    public function store(Request $request)
    {
        //requested data are being stored into "$data" array
        $validatedData = $request->validate([
            'title' => 'required|unique:categories|min:3',
        ]);
        //use laravel's built-in static "create" method with requested data 
        Category::create($validatedData);
        //a confirmation message are being displayed in the application
        session()->flash('message', 'Category Inserted Successfully');
        //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
        return redirect('/categories/create');
    }

    public function show($id)
    {        
        //find a specific category from category table
        $categories = Category::find($id);
        //compact category's data into show route
        return view('backend.categories.show', compact('categories')); 
    }

    public function edit($id)
    {
        //find a specific category, search by requested $id
        $categories = Category::find($id);
        //find a all the latest first products from product table for listview
        $tokens = Category::latest()->get();
        //for changing directory
        $directory = '/categories';
        //compact directory value, category's and token's data into edit route
        return view('backend.categories.edit', compact('categories','tokens', 'directory'));
    }

    public function update(Request $request, $id)
    {
        //find a specific Category, search by requested $id
        $categories = Category::find($id);
        //use "$request->only()" method to get updated value from user
        $validatedData = $request->validate([
            'title' => 'required|unique:categories|min:3',
        ]);        
        //use laravel's built-in "update" method with requested data 
        $categories->update($validatedData);
        //a confirmation message are being displayed in the application
        session()->flash('message', 'Category Updated Successfully');
        //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
        return redirect('/categories/create');

    }

    public function destroy($id)
    {
        //use laravel's built-in static "destroy" method with requested "$id" 
        Category::destroy($id);
        //a confirmation message are being displayed in the application
        session()->flash('message', 'Category Deleted Successfully');
        //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
        return redirect('/categories/create');

    }
}
