<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;


class FrontendController extends Controller
{
	public function frontIndex()
	{
		$tokens = Product::all();
		// $tokens = Product::latest()->get();
		return view('frontend.accessories.index', compact('tokens'));
	}

	public function singleView($id)
	{
        //find a specific product from product table
		$tokens = Product::find($id);
        //compact product's data into show route
		return view('frontend.accessories.singleview', compact('tokens'));    }

	}
