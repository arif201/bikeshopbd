<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
        //find all product info from Product model and the letest one will be in the top
        $tokens = Brand::latest()->get();
        //for changing directory
        $directory = '/brands';
        //compact product's data and directory value into index route 
        return view('backend.brands.index', compact('tokens', 'directory'));
    }
    
    public function create()
    {
        //find all product info from Product model and the letest one will be in the top
        $tokens = Brand::latest()->get();
        //for changing directory
        $directory = '/brands';
        //compact product's data and directory value into index route 
        return view('backend.brands.create', compact('tokens', 'directory'));
    }
    
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:brands|min:3',
        ]);
        //requested data are being stored into "$data" array
        // $data = $request->only('name');
        //use laravel's built-in static "create" method with requested data 
        Brand::create($validatedData);
        //a confirmation message are being displayed in the application
        session()->flash('message', 'Brand Inserted Successfully');
        //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
        return redirect('/brands/create');
    }

    public function show($id)
    {        
        //find a specific category from category table
        $brands = Brand::find($id);
        //compact category's data into show route
        return view('backend.brands.show', compact('brands')); 
    }

    public function edit($id)
    {
        //find a specific category, search by requested $id
        $brands = Brand::find($id);
        //find a all the latest first products from product table for listview
        $tokens = Brand::latest()->get();
        //for changing directory
        $directory = '/brands';
        //compact directory value, category's and token's data into edit route
        return view('backend.brands.edit', compact('brands','tokens', 'directory'));
    }

    public function update(Request $request, $id)
    {
        //find a specific Brand, search by requested $id
        $brands = Brand::find($id);
        //use "$request->only()" method to get updated value from user
        $validatedData = $request->validate([
            'name' => 'required|unique:brands|min:3',
        ]);
        //use laravel's built-in "update" method with requested data 
        $brands->update($validatedData);
        //a confirmation message are being displayed in the application
        session()->flash('message', 'Brand Updated Successfully');
        //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
        return redirect('/brands/create');

    }

    public function destroy($id)
    {
        //use laravel's built-in static "destroy" method with requested "$id" 
        Brand::destroy($id);
        //a confirmation message are being displayed in the application
        session()->flash('message', 'Brand Deleted Successfully');
        //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
        return redirect('/brands/create');

    }
}
