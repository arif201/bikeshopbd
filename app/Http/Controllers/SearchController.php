<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class SearchController extends Controller
{
	public function active_customers() {

		$customers = Customer::all();
		$id = Customer::pluck('id');
		$status = Customer::pluck('status');
		$data =[];
		for($i=0; $i<count($customers); $i++){
			if($status[$i]==1){
				$data[] += $id[$i];
			}
		}
		$tokens = Customer::find($data);
		$directory = '/customers';
		return view('accounce.customers.active_customers', compact('tokens', 'directory'));
	}
	
	public function inactive_customers() {

		$customers = Customer::all();
		$id = Customer::pluck('id');
		$status = Customer::pluck('status');
		$data =[];
		for($i=0; $i<count($customers); $i++){
			if($status[$i]==0){
				$data[] += $id[$i];
			}
		}
		$tokens = Customer::find($data);
		$directory = '/customers';
		return view('accounce.customers.inactive_customers', compact('tokens', 'directory'));
	}


}
