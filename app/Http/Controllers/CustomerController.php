<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        //find all product info from Product model and the letest one will be in the top
        $tokens = Customer::latest()->get();
        // $tokens = Customer::latest()->get();
        //for changing directory
        $directory = '/customers';
        //compact product's data and directory value into index route 
        return view('backend.customers.index', compact('tokens', 'directory'));
    }
    
    public function create()
    {
        //find all product info from Product model and the letest one will be in the top
        $tokens = Customer::latest()->get();
        //for changing directory
        $directory = '/customers';
        //compact product's data and directory value into index route 
        return view('backend.customers.create', compact('tokens', 'directory'));
    }
    
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:3',
            'phone' => 'required|unique:customers|min:7',
            'address' => 'required|max:100',
            'email' => 'nullable',
        ]);

        //use laravel's built-in static "create" method with requested data 
        Customer::create($validatedData);
        //a confirmation message are being displayed in the application
        session()->flash('message', 'Customer Inserted Successfully');
        //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
        return redirect('/customers/create');
    }

    public function show($id)
    {        
        //find a specific category from category table
        $customers = Customer::find($id);
        //compact category's data into show route
        return view('backend.customers.show', compact('customers')); 
    }

    public function edit($id)
    {
        //find a specific category, search by requested $id
        $customers = Customer::find($id);
        //find a all the latest first products from product table for listview
        $tokens = Customer::latest()->get();
        //for changing directory
        $directory = '/customers';
        //compact directory value, category's and token's data into edit route
        return view('backend.customers.edit', compact('customers','tokens', 'directory'));
    }

    public function update(Request $request, $id)
    {
        //find a specific Customer, search by requested $id
        $customers = Customer::find($id);
        
        $validatedData = $request->validate([
            'name' => 'required|min:3',
            'phone' => 'required|unique:customers|min:7',
            'address' => 'required|max:100',
            'email' => 'nullable',
        ]);
        //use laravel's built-in "update" method with requested data 
        $customers->update($validatedData);
        //a confirmation message are being displayed in the application
        session()->flash('message', 'Customer Updated Successfully');
        //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
        return redirect('/customers/create');

    }

    public function destroy($id)
    {
        //use laravel's built-in static "destroy" method with requested "$id" 
        Customer::destroy($id);
        //a confirmation message are being displayed in the application
        session()->flash('message', 'Customer Deleted Successfully');
        //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
        return redirect('/customers/create');

    }

    
}
