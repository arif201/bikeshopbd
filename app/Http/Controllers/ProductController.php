<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Brand;
use Input;
use Storage;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;


class ProductController extends Controller
{
    public function index()
    {
        $tokens = Product::latest()->get();
        $directory = '/products';
        return view('backend.products.index', compact('tokens', 'directory'));
    }
    
    public function create()
    {
        //get all category's id and title from Category model using pluck() method
        $categories = Category::pluck('title','id');
        //get all brand's id and title from Brand model using pluck() method
        $brands = Brand::pluck('name','id');
        //find a all the latest first products from product table for listview
        $tokens = Product::latest()->get();
        //for variable listview directory
        $directory = '/products';
        //compact directory value, category's and token's data into create route
        return view('backend.products.create', compact('categories','brands','tokens', 'directory'));
    }
    
    public function store(ProductRequest $request)
    {   
        $products = new Product(array(
            'category_id' => $request->get('category_id'),
            'brand_id' => $request->get('brand_id'),
            'name'  => $request->get('name'),
            'buying_price' => $request->get('buying_price'),
            'selling_price' => $request->get('selling_price'),
        ));

        $image = Input::file('image');
        $imageName = time().$image->getClientOriginalName();
        $image->move('uploads', $imageName);
        $products->image = $imageName;
        $products->save();
        
        // ............................................
        // $validatedData = $request->validate([
        //     'category_id' => 'required',
        //     'brand_id' => 'required',
        //     'name' => 'required|min:3',
        //     'buying_price' => 'required|min:2',
        //     'selling_price' => 'required|min:2',
        // ]);

        // Product::create($validatedData);

        // $image = Input::file('image');
        // $imageName = $image->getClientOriginalName();
        // $image->move('uploads', $imageName);
        // $request->image = $imageName;

        // $validatedData = $request->validate([
        //     'image' => 'nullable',
        // ]);

        // Product::create($validatedData);

        //..................................................
        // $validatedData = $request->validate([
        //     'category_id' => 'required',
        //     'brand_id' => 'required',
        //     'name' => 'required|min:3',
        //     'buying_price' => 'required|min:2',
        //     'selling_price' => 'required|min:2',
        //     'image' => 'nullable',
        // ]);
        // .................................................

        // a confirmation message are being displayed in the application
        session()->flash('message', 'Product Inserted Successfully');
        //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
        return redirect('/products/create');
    }

    public function show($id)
    {
        //find a specific product from product table
        $products = Product::find($id);
        //compact product's data into show route
        return view('backend.products.show', compact('products')); 
    }

    public function edit($id)
    {
        //find a specific product, search by requested $id
        $products = Product::find($id);
        //get all category's id and title from Category model using pluck() method
        $categories = Category::pluck('title','id');
        //get all brand's id and title from Brand model using pluck() method
        $brands = Brand::pluck('name','id');
        //find a all the latest first products from product table for listview
        $tokens = Product::latest()->get();
        //for changing directory
        $directory = '/products';
        //compact directory value, category's, product's and token's data into edit route
        return view('backend.products.edit', compact('products','categories','brands','tokens', 'directory'));
    }

    public function update(ProductRequest $request, $id)
    {

        $data = request()->except(['_method','_token']);

        if($request->hasFile('image')){
            $image = Input::file('image');
            $imageName = time().$image->getClientOriginalName();
            $image->move('uploads', $imageName);

        }
        
        $data['image'] = $imageName;
         

        Product::where('id', $id)->update($data);
        session()->flash('message', 'Product Updated Successfully');
        return redirect('/products/create');

    }

    public function destroy($id)
    {        
            //use laravel's built-in static "destroy" method with requested "$id" 
        Product::destroy($id);
            //a confirmation message are being displayed in the application
        session()->flash('message', 'Product Deleted Successfully');
            //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
        return redirect('/products/create');
    }
}


































//.............................................................//
// $products = new Product(array(
//             'category_id' => $request->get('category_id'),
//             'brand_id' => $request->get('brand_id'),
//             'name'  => $request->get('name'),
//             'buying_price'  => $request->get('buying_price'),
//             'selling_price'  => $request->get('selling_price'),
//         ));
//         $products->update();

//         if(Input::hasFile('image')){
//          $file = Input::file('image');
//            // $destinationPath = public_path(). '/uploads/';
//          $destinationPath = public_path().'/uploads/';
//          $filename = $file->getClientOriginalName();
//          $file->move($destinationPath, $filename);
//          $products->image =           
//          $destinationPath.$filename;
//          $products->update();
//          session()->flash('message', 'Product Updated Successfully');
//          return redirect('/products/create');
//      }

//      else{
//         $products->update();
//         session()->flash('message', 'Product Updated Successfully');
//         return redirect('/products/create');
//     }
        // $image = $request->image;
        // // $imageName = $image->getClientOriginalName();
        // $image->move('uploads', $imageName);
        // $products->image = $imageName;
        // $products->update();

        // $upload = '/uploads';
        // $file_path = url($upload.'/'.$products->image);
        // if(file_exists('file_path')){
        //     @unlink('file_path');
        // }
        // parent::delete();

        // $products = Product::find($id);
        // //use "$request->only()" method to get updated value from user
        // $data = $request->only('category_id','brand_id','name','buying_price','selling_price','image');
        // //use laravel's built-in "update" method with requested data 
        // $products->update($data);
        //a confirmation message are being displayed in the application
        //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
//.............................................................//


// public function store(Request $request)
//     {
//         //Upload selected image to the Storage folder
        // $image = $request->file('image');
        // $filename = $image->getClientOriginalName();
//         $upload_image = Storage::put('upload/images/'.$filename, 
//             file_get_contents($request->file('image')->getRealPath()));

//         //requested data are being stored into "$data" array
//         $data = $request->only('category_id','brand_id','name','buying_price','selling_price','image');

//         $data->image = $filename;
//         //use laravel's built-in static "create" method with requested data 
//         Product::create($data);

//         //a confirmation message are being displayed in the application
//         session()->flash('message', 'Product Inserted Successfully');
//         //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
//         return redirect('/products/create');
//     }



// public function store(Request $request)
//     {
//         $products = new Product(array(
//             'category_id' => $request->get('category_id'),
//             'brand_id' => $request->get('brand_id'),
//             'name'  => $request->get('name'),
//             'buying_price'  => $request->get('buying_price'),
//             'selling_price'  => $request->get('selling_price'),
//         ));

//         $products->save();
//         $image = $request->file('image');
//         $imageName = $image->getClientOriginalName();
//         Storage::put('upload/images/'.$imageName, 
//             file_get_contents($request->file('image')->getRealPath()));
//         $products->image = $imageName;
//         $products->save();
//         // a confirmation message are being displayed in the application
//         session()->flash('message', 'Product Inserted Successfully');
//         //As store method stores data into DB and returns nothing, we should redirect a particur page.............. 
//         return redirect('/products/create');
//     }


        // if(Input::hasFile('file')){
        //     $image = Input::file('image');
        //     $imageName = $image->getClientOriginalName();
        //     $image->move('uploads', $imageName);
        //     $products->image = $imageName;
        //     $products->save();
        // }

// if(Input::hasFile('image'))
//     {
//      File::delete(public_path() . '/images/profile/', $article->file->name); // Delete old flyer
// }

// $filename = $request->image->hashName();
        // $request->image->store('uploads');

        // $image = Input::file('image');
        // $imageName = $image->getClientOriginalName();
        // $image->move('uploads/', $imageName);
        // $products->image = $imageName;

