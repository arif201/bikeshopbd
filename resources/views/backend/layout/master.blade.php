<!DOCTYPE HTML>
<html>
<head>
	@include('backend.layout.head')
</head> 
<body class="sticky-header left-side-collapsed">
	@include('backend.layout.sidebar')
	<div class="main-content">
		@include('backend.layout.header')
		<div id="page-wrapper">
			<div class="graphs">
				<h3 class="blank1">@yield('page_title')</h3>		
				@yield('content')
			</div>
		</div>
	</div>
	<script src="{{ asset('backend/js/scripts.js') }}"></script>
	<script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('backend/js/jquery-1.10.2.min.js') }}"></script>
	<script src="{{ asset('backend/js/jquery.nicescroll.js') }}"></script>
	<script src="{{ asset('backend/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('backend/js/jquery.dataTables.min.js') }}"></script>

</body>


</html>
