<title>Stock Management System @yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="{{ asset('backend/css/bootstrap.min.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ asset('backend/css/style.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ asset('backend/css/font-awesome.css') }}" rel="stylesheet"><link href="{{ asset('backend/css/icon-font.min.css') }}" rel="stylesheet" type='text/css' />
<link href="{{ asset('backend/css/animate.css') }}" rel="stylesheet" type="text/css" media="all">
<link href="{{ asset('backend/css/jquery.dataTables.css') }}" rel="stylesheet" type="text/css" media="all">
<link href="{{ asset('backend/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" media="all">
@stack('styles')

<script src="{{ asset('backend/js/scripts.js') }}"></script>
<script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('backend/js/jquery-1.10.2.min.js') }}"></script>
<script src="{{ asset('backend/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('backend/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/js/jquery.dataTables.min.js') }}"></script>
@stack('javascripts')
