<!-- left side start-->
<div class="left-side sticky-left-side">

	{{-- logo and iconic logo start --}}
	<div class="logo">
		<h1><a href="#">Easy <span>Admin</span></a></h1>
	</div>
	<div class="logo-icon text-center">
		<a href="index.html"><i class="lnr lnr-home" style="color:#476642"></i> </a>
	</div>

	{{-- logo and iconic logo end --}}

	<div class="left-side-inner">

		<!--sidebar nav start-->
		<ul class="nav nav-pills nav-stacked custom-nav">

			<li class="active">
				<a href="#">
					<i class="fa fa-power-off" style="color:#476642"></i>
					<span>Dashboard</span>
				</a>
			</li>
			
			
			<li>
				<a href="{{ url('/customers') }}">
					<i class="fa fa-android" style="color:#476642"></i>
					<span>Customer</span></a>
				</li>

				<li>
					<a href="{{ url('/categories') }}">
						<i class="fa fa-list" style="color:#476642"></i>
						<span>Category</span></a>
					</li>

					<li>
						<a href="{{ url('/brands') }}">
							<i class="fa fa-tags" style="color:#476642"></i>
							<span>Brand</span></a>
						</li>

						<li>
							<a href="{{ url('/products') }}">
								<i class="fa fa-archive" style="color:#476642"></i>
								<span>Product</span></a>
							</li>

							<li>
								<a href="#">
									<i class="fa fa-gavel" style="color:#476642"></i>
									<span>Order</span></a>
								</li>


								<li>
									<a href="{{ url('/payment') }}">
										<i class="fa fa-dollar" style="color:#476642"></i>
										<span>Payment</span></a>
									</li>

									<li>
										<a href="#">
											<i class="fa fa-truck" style="color:#476642"></i>
											<span>Delivery</span></a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-users" style="color:#476642"></i>
												<span>User</span></a>
											</li>

											<li>
												<a href="#">
													<i class="fa fa-cogs" style="color:#476642"></i>
													<span>Settings</span></a>
												</li>	

											</ul>
										</div>
									</div>

























{{-- 
			<li>
				<a href="{{ url('/carts') }}">
					<i class="fa fa-shopping-cart" style="color:#31a03e"></i>
					<span>Cart</span></a>
			</li>
			
			<li>
				<a href="{{ url('/suppliers') }}">
					<i class="fa fa-user"></i>
					<span>Supplier</span></a>
			</li>

			<li>
				<a href="#">
					<i class="fa fa-file"></i>
					<span>Profile</span></a>
			</li> 

			<li>
				<a href="#">
					<i class="lnr lnr-home"></i>
					<span>Home</span>
				</a>
			</li>

			<li>
				<a href="#">
					<i class="fa fa-barcode" style="color:#476642"></i>
					<span>Refund</span></a>
			</li>

			<li>
				<a href="#">
					<i class="fa fa-dollar" style="color:#476642"></i>
					<span>Expenditure</span></a>
			</li>

			<li>
				<a href="{{ url('/payment_methods') }}">
					<i class="fa fa-briefcase" style="color:#476642"></i>
					<span>Payment Methods</span></a>
			</li>
			
			
			--}}

			
			

{{-- 
					<li class="menu-list">
						<a href="#"><i class="lnr lnr-cog"></i>
							<span>Components</span></a>
							<ul class="sub-menu-list">
								<li><a href="grids.html">Grids</a> </li>
								<li><a href="widgets.html">Widgets</a></li>
							</ul>
						</li>
						<li><a href="forms.html"><i class="lnr lnr-spell-check"></i> <span>Forms</span></a></li>
						<li><a href="tables.html"><i class="lnr lnr-menu"></i> <span>Tables</span></a></li>              
						<li class="menu-list"><a href="#"><i class="lnr lnr-envelope"></i> <span>MailBox</span></a>
							<ul class="sub-menu-list">
								<li><a href="inbox.html">Inbox</a> </li>
								<li><a href="compose-mail.html">Compose Mail</a></li>
							</ul>
						</li>      
						<li class="menu-list"><a href="#"><i class="lnr lnr-indent-increase"></i> <span>Menu Levels</span></a>  
							<ul class="sub-menu-list">
								<li><a href="charts.html">Basic Charts</a> </li>
							</ul>
						</li>
						<li><a href="codes.html"><i class="lnr lnr-pencil"></i> <span>Typography</span></a></li>
						<li><a href="media.html"><i class="lnr lnr-select"></i> <span>Media Css</span></a></li>
						<li class="menu-list"><a href="#"><i class="lnr lnr-book"></i>  <span>Pages</span></a> 
							<ul class="sub-menu-list">
								<li><a href="sign-in.html">Sign In</a> </li>
								<li><a href="sign-up.html">Sign Up</a></li>
								<li><a href="blank_page.html">Blank Page</a></li>
							</ul>
						</li> --}}
						<!--sidebar nav end-->

						<!-- left side end-->

