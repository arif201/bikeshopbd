{{-- This code is for viewing a table or list right side of the application --}}
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTable">

  <thead>
    <tr class="success">
      <th>SN</th>
      <th>Name</th>
      <th>Phone Number</th>
      <th>Address</th>
      <th>Email Address</th>
      <th>Action</th>
    </tr>
  </thead>

  <tbody>
    @php $sl=0; @endphp
    @foreach($tokens as $token)
    <tr>
      <td>{{++$sl}}</td>
      <td>{{$token->name}}</td>
      <td>{{$token->phone}}</td>
      <td>{{$token->email}}</td>
      <td>{{$token->address}}</td>
      <td>
        <a href="{{ url($directory.'/'.$token->id) }}">Show |</a>
        <a href="{{ url($directory.'/'.$token->id.'/edit') }}">Edit |</a>


        {!! Form::open([
          'url'=> [ $directory, $token->id],
          'method'=> 'delete'
          ]) !!}
          {{ Form::submit('Delete') }}
          {!! Form::close() !!}

        </td>
      </tr>
      @endforeach 
    </tbody>
  </table>

  @push('javascripts')
  <script>
    $(document).ready( function () {
      $('#dataTable').DataTable();
    });
  </script>
  @endpush

