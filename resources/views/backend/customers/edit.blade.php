@extends('backend.layout.master')
@section('title', ' :: Update Customer')
@section('page_title', 'Update an existing Customer')
@section('content')
<div class="row">

	<div class="col-lg-4">
		<div class="panel-heading">

			{{-- for session message --}}
			@if(session()->has('message'))
			{{session('message')}}
			@endif

			{{-- Take value and call update method using form facade --}}
			{{-- send "$customers" for ListView and "$token" for specified info  --}}
			{!! Form::model($customers, ['url' => ['/customers', $customers->id], 'method'=>'put']) !!}   
			{{-- form's text field for name attribute--}}
			{{ Form::text('name', $customers->name, ['class'=>'form-control', 'id'=>'name']) }}
			{{-- form's text field for phone attribute--}}
			{{ Form::text('phone', $customers->phone, ['class'=>'form-control', 'id'=>'phone']) }}
			{{-- form's text field for address attribute--}}
			{{ Form::text('address', $customers->address, ['class'=>'form-control', 'id'=>'address']) }}{{-- form's text field for email attribute--}}
			{{ Form::text('email', $customers->email, ['class'=>'form-control', 'id'=>'email']) }}

			@foreach($errors->all() as $message)
			{{ $message }}<br>
			{{-- <pre>{{ $message }}</pre> --}}
			@endforeach

			<hr>
			
			{{-- form's submit button --}}
			{{ Form::submit('Update', ['class'=>'btn btn-info btn-xs']) }}
			{{-- to close a form --}}
			{!! Form::close()!!}
		</div>
	</div>

	<div class="col-lg-8 pull-right">
		<div class="panel-body">
			@include('backend.customers.listview')
		</div>
	</div>

</div>
@endsection
