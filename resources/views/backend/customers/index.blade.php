@extends('backend.layout.master')

@section('title', ' :: Index of Customers')
@section('page_title', 'List Customers')

@section('content')
<div class="row">

    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <a href="{{ url('/customers/create') }}" 
                <i class="fa fa-plus"></i> Add New</a>
            </div>
            <hr>
            <!-- /.panel-heading -->
            <div class="panel-body">
                @include('backend.customers.listview')
            </div>    
        </div>
    </div>

    
    @endsection

