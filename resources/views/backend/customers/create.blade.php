@extends('backend.layout.master')
@section('title', ' :: Add a new Customer')
@section('page_title', 'Add a new Customer')
@section('content')
<div class="row">

  <div class="col-lg-4">
    <div class="panel-heading">

      {{-- for session message --}}
      @if(session()->has('message'))
      {{session('message')}}
      @endif


      {{-- call store method using form facade --}}
      {!!Form::open(['url'=>'/customers','method'=> 'post']) !!}
      {{-- form's text field --}}
      {{-- form's text field for name attribute--}}
      {{ Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Do You have a Name?']) }}
      {{-- form's text field for phone attribute--}}
      {{ Form::text('phone', null, ['class'=>'form-control', 'id'=>'phone', 'placeholder'=>'Phone Number Please']) }}
      {{-- form's text field for address attribute--}}
      {{ Form::text('address', null, ['class'=>'form-control', 'id'=>'address', 'placeholder'=>'Detail Address' ]) }}
      {{-- form's text field for email attribute--}}
      {{ Form::text('email', null, ['class'=>'form-control', 'id'=>'email', 'placeholder'=>'Enter an Email Address if any...']) }}
      
      @foreach($errors->all() as $message)
      {{ $message }}<br>
      {{-- <pre>{{ $message }}</pre> --}}
      @endforeach

      <hr>
      
      {{-- form's submit button --}}
      {{ Form::submit('Add New', ['class'=>'btn btn-info btn-xs']) }}
      {{-- to close a form --}}
      {!! Form::close()!!}
    </div>
  </div>

  <div class="col-lg-8 pull-right">
    <div class="panel-body">
      @include('backend.customers.listview')
    </div>
  </div>

</div>
@endsection
