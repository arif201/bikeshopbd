  {{-- This code is for viewing a table or list right side of the application --}}
  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">

  <thead>
  <tr class="success">
  <th>SN</th>
  <th>Product Name</th>
  <th>Product's Image</th>
  <th>Category Title</th>
  <th>Brand Name</th>
  <th>Price(Buying) BDT</th>
  <th>Price(Selling) BDT</th>
  <th>Action</th>
  </tr>
  </thead>

  <tbody>
    @php $sl=0; @endphp
    @foreach($tokens as $token)
  <tr>
  <td>{{++$sl}}</td>
  <td>{{$token->name}}</td>
  <td><img src="/uploads/{{$token->image}}" height = "90px" width = "67px" ></td>
  {{-- <td><img src="{{ url($upload.'/'.$token->image) }}" height = "90px" width = "67px" ></td> --}}
  <td>{{$token->category->title}}</td>
  <td>{{$token->brand->name}}</td>
  <td>{{$token->buying_price}}</td>
  <td>{{$token->selling_price}}</td>
  <td>
  <a href="{{ url($directory.'/'.$token->id) }}">Show |</a>
  <a href="{{ url($directory.'/'.$token->id.'/edit') }}">Edit |</a>

  {!! Form::open([ 'url'=> [ $directory, $token->id], 'method'=> 'delete' ]) !!}
  {{ Form::submit('Delete') }}
  {!! Form::close() !!}

  </td>
  </tr>
  @endforeach 
  </tbody>
  </table>
  