@extends('backend.layout.master')
@section('title', ' :: Index of Products')
@section('page_title', 'Product List')

@section('content')
<div class="row">

    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <a href="{{ url('/products/create') }}" 
                <i class="fa fa-plus"></i> Add New</a>

            </div>

            <!-- /.panel-heading -->
            <div class="panel-body">
                @include('backend.products.listview')
            </div>    
        </div>
    </div>
@endsection
