@extends('backend.layout.master')
@section('title', ' :: Add a new Product')
@section('page_title', 'Add a new Product')
@section('content')
<div class="row">

  <div class="col-lg-4">
    <div class="panel-heading">

      {{-- for session message --}}
      @if(session()->has('message'))
      {{session('message')}}
      @endif


      {{-- call store method using form facade --}}
      {!!Form::open(['url'=>'/products','method'=> 'post','enctype'=> 'multipart/form-data']) !!}

      {{-- form's dropdown menu --}}
      {!! Form::select('category_id', $categories ,null, ["placeholder"=>"Select a Category for Your Product","class" => "form-control", 'required', 'autofocus']) !!}

      {{-- form's dropdown menu --}}
      {!! Form::select('brand_id', $brands ,null, ["placeholder"=>"Select a Brand for Your Product","class" => "form-control", 'required', 'autofocus']) !!}

      {{-- form's text field --}}
      {{ Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Please Enter Products Name']) }}
      
      {{-- form's text field --}}
      {{ Form::text('buying_price', null, ['class'=>'form-control', 'id'=>'buying_price', 'placeholder'=>'Product Buying Price']) }}
      
      {{-- form's text field --}}
      {{ Form::text('selling_price', null, ['class'=>'form-control', 'id'=>'selling_price', 'placeholder'=>'Product Selling Price']) }}
      <br>
      {{ Form::input('file', 'image') }}
      <br>

      @foreach($errors->all() as $message)
      {{ $message }}<br>
      {{-- <pre>{{ $message }}</pre> --}}
      @endforeach

      <hr>
      
      {{-- form's submit button --}}
      {{ Form::submit('Add New', ['class'=>'btn btn-info btn-xs']) }}
      {{-- to close a form --}}
      {!! Form::close()!!}
    </div>
  </div>

  <div class="col-lg-8 pull-right">
    <div class="panel-body">
      @include('backend.products.listview')
    </div>
  </div>

</div>
@endsection
