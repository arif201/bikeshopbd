@extends('backend.layout.master')
@section('title', ' :: Update Products')
@section('page_title', 'Update an existing Product')
@section('content')
<div class="row">

	<div class="col-lg-4">
		<div class="panel-heading">

			{{-- for session message --}}
			@if(session()->has('message'))
			{{session('message')}}
			@endif

			{{-- Take value and call update method using form facade --}}
			{!! Form::model($products, ['url' => ['/products', $products->id], 'method'=>'put', 'files'=>true]) !!}   

      {{-- form's dropdown menu --}}
      {!! Form::select('category_id', $categories, null, ["class" => "form-control", 'required', 'autofocus']) !!}

      {{-- form's dropdown menu --}}
      {!! Form::select('brand_id', $brands, null, ["class" => "form-control", 'required', 'autofocus']) !!}

      {{-- form's text field --}}
      {{ Form::text('name', $products->name, ['class'=>'form-control', 'id'=>'name']) }}

      {{-- form's text field --}}
      {{ Form::text('buying_price', $products->buying_price, ['class'=>'form-control', 'id'=>'buying_price', 'placeholder'=>'Product Buying Price']) }}
      
      {{-- form's text field --}}
      {{ Form::text('selling_price', $products->selling_price, ['class'=>'form-control', 'id'=>'selling_price', 'placeholder'=>'Product Selling Price']) }}

      {{-- form's file input field --}}
      {{ Form::file('image', ['class' => 'field']) }}

      @foreach($errors->all() as $message)
      {{ $message }}
      @endforeach

      <hr>

      {{-- form's submit button --}}
      {{ Form::submit('Update', ['class'=>'btn btn-info btn-xs']) }}
      {{-- to close a form --}}
      {!! Form::close()!!}
    </div>
  </div>

  <div class="col-lg-8 pull-right">
    <div class="panel-body">
      @include('backend.products.listview')
    </div>
  </div>  


</div>
@endsection
