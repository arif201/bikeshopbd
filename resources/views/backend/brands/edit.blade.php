@extends('backend.layout.master')
@section('title', ' :: Update Brands')
@section('page_title', 'Update an existing Brand')
@section('content')
<div class="row">

	<div class="col-lg-4">
		<div class="panel-heading">

			{{-- for session message --}}
			@if(session()->has('message'))
			{{session('message')}}
			@endif

			{{-- Take value and call update method using form facade --}}
			{{-- send "$brands" for ListView and "$token" for specified info  --}}
			{!! Form::model($brands, ['url' => ['/brands', $brands->id], 'method'=>'put']) !!}   
			{{-- form's text field --}}
			{{ Form::text('name', $brands->name, ['class'=>'form-control', 'id'=>'name']) }}

			@foreach($errors->all() as $message)
			{{ $message }}
			@endforeach

			<hr>
			{{-- form's submit button --}}
			{{ Form::submit('Update', ['class'=>'btn btn-info btn-xs']) }}
			{{-- to close a form --}}
			{!! Form::close()!!}
		</div>
	</div>

	<div class="col-lg-8 pull-right">
		<div class="panel-body">
			@include('backend.brands.listview')
		</div>
	</div>

</div>
@endsection
