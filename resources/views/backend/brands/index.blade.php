@extends('backend.layout.master')
@section('title', ' :: Index of Brands')
@section('page_title', 'List of Brands')

@section('content')
<div class="row">

    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <a href="{{ route('brands.create') }}" 
                <i class="fa fa-plus"></i> Add New</a>

            </div>

            <!-- /.panel-heading -->
            <div class="panel-body">
                @include('backend.brands.listview')
            </div>    
        </div>
    </div>
@endsection
