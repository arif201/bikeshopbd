@extends('backend.layout.master')
@section('title', ' :: Index of Categories')
@section('page_title', 'List Categories')

@section('content')
<div class="row">

    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <a href="{{ url('/categories/create') }}" 
                <i class="fa fa-plus"></i> Add New</a>

            </div>

            <!-- /.panel-heading -->
            <div class="panel-body">
                @include('backend.categories.listview')
            </div>    
        </div>
    </div>
@endsection
