@extends('backend.layout.master')
@section('title', ' :: Add a new Category')
@section('page_title', 'Add a new Category')
@section('content')
<div class="row">

  <div class="col-lg-4">
    <div class="panel-heading">

      {{-- for session message --}}
      @if(session()->has('message'))
      {{session('message')}}
      @endif


      {{-- call store method using form facade --}}
      {!!Form::open(['url'=>'/categories','method'=> 'post']) !!}
      {{-- form's text field --}}
      {{ Form::text('title', null, ['class'=>'form-control', 'id'=>'title', 'placeholder'=>'Enter title']) }}

      @foreach($errors->all() as $message)
      {{ $message }}
      @endforeach

      <hr>
      {{-- form's submit button --}}
      {{ Form::submit('Add New', ['class'=>'btn btn-info btn-xs']) }}
      {{-- to close a form --}}
      {!! Form::close()!!}
    </div>
  </div>

  <div class="col-lg-8 pull-right">
    <div class="panel-body">
      @include('backend.categories.listview')
    </div>
  </div>

</div>
@endsection
