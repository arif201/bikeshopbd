@extends('frontend.layout.master')
@section('page_title', 'Single Page View')
@section('content')
<div class="product-head">
	<a href="#">Home</a> <span>::</span>	
</div>

<div class="details-left-info pull-right">
	<h3>{{$tokens->name}}</h3>
	<h3>৳ {{$tokens->selling_price}}</h3>
	<h4></h4>
	<div class="btn_form">
		<a href="#">buy now</a>
		<a href="#">ADD TO CART</a>
	</div>
	<div class="bike-type">
		<p>TYPE  ::<a href="#">MOUNTAIN BIKE</a></p>
		<p>BRAND  ::<a href="#">SPORTS SCOTTY</a></p>
	</div>
	<h5>Description  ::</h5>
	<p class="desc">The first mechanically-propelled, two-wheeled vehicle may have been built by Kirkpatrick MacMillan, a Scottish blacksmith, in 1839, although the claim is often disputed. He is also associated with the first recorded instance of a cycling traffic offense, when a Glasgow newspaper in 1842 reported an accident in which an anonymous "gentleman from Dumfries-shire... bestride a velocipede... of ingenious design" knocked over a little girl in Glasgow and was fined five
	The word bicycle first appeared in English print in The Daily News in 1868, to describe "Bysicles and trysicles" on the "Champs Elysées and Bois de Boulogne.</p>
</div>

<div class="details-left-slider">
	<div class="grid images_3_of_2">
		<ul id="etalage" class="etalage" style="display: block; width: 400px; height: 537px;">

			<li style="display: none; background-image: none; opacity: 0;">
				<a href="#">
					<img 
					class="etalage_thumb_image" 
					id="zoom01"
					data-zoom-image="/uploads/{{$tokens->image}}"
					src="/uploads/{{$tokens->image}}" 
					style="display: inline; 
					width: 380px; 
					height: 380px; 
					opacity: 1;">
				</a>
			</li>

			<li class="etalage_thumb thumb_2 etalage_thumb_active" style="background-image: none; display: list-item; opacity: 1;">
				<img 
				class="etalage_thumb_image"
				id="zoom01"
				data-zoom-image="/uploads/{{$tokens->image}}" 
				src="/uploads/{{$tokens->image}}" 
				style="display: inline; 
				width: 380px; 
				height: 380px; 
				opacity: 1;">
			</li>
		</ul>
	</div>
</div>

<div class="clearfix"></div>

@endsection
{{-- 
@section('styles')
<link rel="stylesheet" href="{{ asset('frontend/css/miniZoomPan.css') }}" type="text/css" media="screen" />
@endsection

@section('javascripts')

<script 
type="text/javascript" 
src="{{ asset('frontend/js/jquery-3.3.1.js') }}">
</script>

<script 
type="text/javascript" 
src="{{ asset('frontend/css/miniZoomPan.js') }}">
</script>

<script type= "text/javascript">
/*<![CDATA[*/
$(function() {
	$("#zoom01, #zoom02, #zoom03").miniZoomPan({
			sW: 200,
			sH: 250,
			lW: 370,
			lH: 462
		})
	
		// $("#bacteria").miniZoomPan({
		// 	sW: 125,
		// 	sH: 125,
		// 	lW: 500,
		// 	lH: 500,
		// 	frameColor: "#000",
		// 	frameWidth: 60,
		// 	loaderContent: "<img src='spinner.gif' />"
		// })

		$("#pano").miniZoomPan({
			sW: 515,
			sH: 100,
			lW: 3555,
			lH: 690
		})
});
/*]]>*/
</script>



@endsection

 --}}