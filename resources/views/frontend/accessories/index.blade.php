@extends('frontend.layout.master')
@section('page_title', 'Bike Shop Accessories')
@section('frontend_title', 'BIKE-ACCESSORIES')
@include('frontend.layout.sidebar')
@section('content')
<div class="top">
	<ul>
		<li><a href="#">home</a></li>
		<li><a href="#"> / </a></li>
		<li><a href="#">parts</a></li>
	</ul>				 
</div>

<div class="bike-apparels">
	@foreach($tokens as $token)
		<div class="parts1">
		<a href="#"></a>
		<div class="part-sec">

			<a href="/singleview/{{$token->id}}">
				<img 
				src="/uploads/{{$token->image}}" 
				height = "300px" 
				width = "300px" 
				>
			</a>

			<div class="part-info"><a href="/singleview/{{$token->id}}"></a>
			<a href="/singleview/{{$token->id}}">
				<h5>{{$token->name}}
					<span>৳ {{$token->selling_price}}</span>
				</h5>
			</a>

			<a class="add-cart" href="#">Quick View</a>
			<a class="qck" href="#">BUY NOW</a>
		</div>
		<div class="clearfix"></div>
</div>
@endforeach 

@endsection
