@section('sidebar')
<div class="rsidebar span_1_of_left">
	<section class="sky-form">
		<div class="product_right">
			<h3 class="m_2">Categories</h3>
			<div class="metro"><span class="old"><select class="" tabindex="10" data-settings="{&quot;wrapperClass&quot;:&quot;metro&quot;}" id="EasyDropDown72517E">
				<option value="0">Frames</option>	
				<option value="1">Back Packs</option>
				<option value="2">Frame Bags</option>
				<option value="3">Panniers </option>
				<option value="4">Saddle Bags</option>								
			</select></span><span class="selected">Frames</span><span class="carat"></span><div><ul><li class="active">Frames</li><li>Back Packs</li><li>Frame Bags</li><li>Panniers </li><li>Saddle Bags</li></ul></div></div>
			<div class="metro"><span class="old"><select class="" tabindex="50" data-settings="{&quot;wrapperClass&quot;:&quot;metro&quot;}" id="EasyDropDown9F3FA8">
				<option value="1">Body Armour</option>
				<option value="2">Sub Category1</option>
				<option value="3">Sub Category2</option>
				<option value="4">Sub Category3</option>
			</select></span><span class="selected">Body Armour</span><span class="carat"></span><div><ul><li class="active">Body Armour</li><li>Sub Category1</li><li>Sub Category2</li><li>Sub Category3</li></ul></div></div>
			<div class="metro"><span class="old"><select class="" tabindex="8" data-settings="{&quot;wrapperClass&quot;:&quot;metro&quot;}" id="EasyDropDown9A193F">
				<option value="1">Tools</option>
				<option value="2">Sub Category1</option>
				<option value="3">Sub Category2</option>
				<option value="4">Sub Category3</option>
			</select></span><span class="selected">Tools</span><span class="carat"></span><div><ul><li class="active">Tools</li><li>Sub Category1</li><li>Sub Category2</li><li>Sub Category3</li></ul></div></div>
			<div class="metro"><span class="old"><select class="" tabindex="8" data-settings="{&quot;wrapperClass&quot;:&quot;metro&quot;}" id="EasyDropDown4C039B">
				<option value="1">Services</option>
				<option value="2">Sub Category1</option>
				<option value="3">Sub Category2</option>
				<option value="4">Sub Category3</option>
			</select></span><span class="selected">Services</span><span class="carat"></span><div><ul><li class="active">Services</li><li>Sub Category1</li><li>Sub Category2</li><li>Sub Category3</li></ul></div></div>
			<div class="metro"><span class="old"><select class="" tabindex="8" data-settings="{&quot;wrapperClass&quot;:&quot;metro&quot;}" id="EasyDropDown5F37DF">
				<option value="1">Materials</option>
				<option value="2">Sub Category1</option>
				<option value="3">Sub Category2</option>
				<option value="4">Sub Category3</option>
			</select></span><span class="selected">Materials</span><span class="carat"></span><div><ul><li class="active">Materials</li><li>Sub Category1</li><li>Sub Category2</li><li>Sub Category3</li></ul></div></div>
		</div>

		<h4>components</h4>
		<div class="row row1 scroll-pane">
			<div class="col col-4">
				<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Frames(20)</label>
			</div>
			<div class="col col-4">
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Foks, Suspensions (48)</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Breaks and Pedals (45)</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Tires,Tubes,Wheels (45)</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Serevice Parts(12)</label>
			</div>
		</div>
		<h4>Apparels</h4>
		<div class="row row1 scroll-pane">
			<div class="col col-4">
				<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Locks (20)</label>
			</div>
			<div class="col col-4">
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Speed Cassette (5)</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Bike Pedals (7)</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Handels (2)</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Other (50)</label>
			</div>
		</div>
	</section>
	<section class="sky-form">
		<h4>Brand</h4>
		<div class="row row1 scroll-pane">
			<div class="col col-4">
				<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Lezyne</label>
			</div>
			<div class="col col-4">
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Marzocchi</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>EBC</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Oakley</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Jagwire</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Yeti Cycles</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Vee Rubber</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Zumba</label>
			</div>
		</div>
	</section>		      
	<section class="sky-form">
		<h4>Price</h4>
		<div class="row row1 scroll-pane">
			<div class="col col-4">
				<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>$50.00 and Under (30)</label>
			</div>
			<div class="col col-4">
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>$100.00 and Under (30)</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>$200.00 and Under (30)</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>$300.00 and Under (30)</label>
				<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>$400.00 and Under (30)</label>
			</div>
		</div>
	</section>		       
</div>
@endsection
