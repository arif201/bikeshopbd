<!DOCTYPE html>
<html>
<head>
	@include('frontend.layout.head')
</head>
<body>
	
	@include('frontend.layout.header')

	{{-- <div class="parts"> --}}
		<div class="container">
			<h3>@yield('frontend_title') </h3>
			<div class="bike-parts-sec">
				@yield('sidebar')
				@yield('content')
				<div class="clearfix"></div>
			</div>
		</div>

		@include('frontend.layout.footer')
	</body>
	</html>