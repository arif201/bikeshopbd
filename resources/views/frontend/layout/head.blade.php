<title>@yield('page_title')</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Bike-shop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />

<link href="{{ asset('frontend/css/bootstrap.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ asset('frontend/css/form.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('frontend/css/nav.css') }}" rel="stylesheet" type="text/css" media="all"/>
@yield('styles')

<script src="{{ asset('frontend/js/jquery.easydropdown.js') }}"></script>
<script src="{{ asset('frontend/js/scripts.js') }}" type="text/javascript"></script>
<script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
@yield('javascripts')

