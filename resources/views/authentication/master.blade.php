<!DOCTYPE HTML>
<html>

<head>
	<title> @yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/auth_style.css') }}">
	<link href="{{ asset('css/bootstrap.min.css') }}" rel='stylesheet' type='text/css' />
	@stack('style')
</head> 

<body>	
	<div class="container">

		@include('authentication.top-menu')
		@yield('content')

	</div>
</body>
</html>
