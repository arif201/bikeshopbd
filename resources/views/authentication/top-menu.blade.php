<div class="heared clearfix">

	<nav class="navbar navbar-fixed-top">
		<div class="container-fluid">

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

				<ul class="nav navbar-nav navbar-right">
					@if(Sentinel::check())
					<li role="presentation">
						{!!Form::open(['url'=>'/logout','method'=> 'post','id'=> 'logout-form']) !!}
						<a href="#" onclick="document.getElementById('logout-form').submit()">Logout</a>
						{!! Form::close()!!}
					</li>
					@else
					<li role="presentation"><a href="/login">Login</a></li>
					<li role="presentation"><a href="/register">Register</a></li>
					@endif
					
				</ul>

			</div>
		</div>
	</nav>

	<h3 class="text-muted">
		@if(Sentinel::check())
		Hello, {{Sentinel::getUser()->first_name}}
		@else
			Please Sing In...........
		@endif
	</h3>

</div>