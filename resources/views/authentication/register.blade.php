@extends('authentication.master')
@section('title', 'Register')
@section('content')
<div class="loginBox">
	<h2>Register</h2>
	{!!Form::open(['url'=>'/register','method'=> 'post']) !!}
	{{ Form::text('email', null, ['id'=>'email', 'placeholder'=>'Enter Email Address', 'required' => 'required']) }}
	{{ Form::text('first_name', null, ['id'=>'first_name', 'placeholder'=>'Enter Your First Name', 'required' => 'required']) }}
	{{ Form::text('last_name', null, ['id'=>'last_name', 'placeholder'=>'Enter Your Last Name', 'required' => 'required']) }}
	{{ Form::password('password', ['id'=>'password', 'placeholder'=>'Enter A Password', 'required' => 'required']) }}
	{{ Form::password('confirm_password', ['id'=>'confirm_password', 'placeholder'=>'Confirm Password', 'required' => 'required']) }}
	{{ Form::submit('Sign Up') }}
	{!! Form::close()!!}
</div>
@endsection	
