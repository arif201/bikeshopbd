@extends('authentication.master')
@section('title', 'login')

@push('style')
<style>
.loginBox
{
	height: 460px;
} 
</style>
@endpush

@section('content')
<div class="loginBox">
	<h2>Login</h2>
	{!!Form::open(['url'=>'/login','method'=> 'post']) !!}
	{{ Form::text('email', null, ['id'=>'email', 'placeholder'=>'Enter Your Email Address', 'required' => 'required']) }}
	{{ Form::password('password', ['id'=>'password', 'placeholder'=>'Got a Password, Huh?', 'required' => 'required']) }}
	{{ Form::submit('Sign In') }}
	<a href="#">Forget Password</a>

	{!! Form::close()!!}
</div>
@endsection	
